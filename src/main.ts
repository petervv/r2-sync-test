import { S3Client } from '@aws-sdk/client-s3';
import dotenv from 'dotenv';
import path from 'path';
import { S3SyncClient } from 's3-sync-client';
import { generateSitemaps } from './generate-sitemaps.js';
import { existsSync, readdirSync } from 'fs';

// Load environment variables from .env file
dotenv.config();

const AWS_BUCKET_NAME = process.env.AWS_BUCKET_NAME!;
const AWS_REGION = process.env.AWS_REGION!;
const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID!;
const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY!;
const ENDPOINT_URL = process.env.ENDPOINT_URL!;
const FOLDER_NAME = 'sitemaps';
const REMOTE_FOLDER_NAME = 'sitemaps-test';
const LOCAL_DIRECTORY = path.resolve(FOLDER_NAME);

const client = new S3SyncClient({
    client: new S3Client({
        region: AWS_REGION,
        credentials: {
            accessKeyId: AWS_ACCESS_KEY_ID,
            secretAccessKey: AWS_SECRET_ACCESS_KEY,
        },
        endpoint: ENDPOINT_URL,
    }),
});

async function syncLocalToS3(): Promise<void> {
    console.log(
        `Starting sync from ${LOCAL_DIRECTORY} => s3://${AWS_BUCKET_NAME}/${REMOTE_FOLDER_NAME}...`,
    );

    console.log(
        `Files in ${LOCAL_DIRECTORY}: ${readdirSync(LOCAL_DIRECTORY).length}`,
    );

    // Sync local folder to S3
    await client.sync(
        LOCAL_DIRECTORY,
        `s3://${AWS_BUCKET_NAME}/${REMOTE_FOLDER_NAME}`,
        { del: true },
    );

    console.log('Sync completed!');
}

if (!existsSync(FOLDER_NAME)) {
    await generateSitemaps();
}

await syncLocalToS3().catch(console.error);
