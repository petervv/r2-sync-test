import { createWriteStream, existsSync, mkdirSync } from 'fs';
import { join } from 'path';
import { createGzip } from 'zlib';

// Define the directory where sitemaps will be saved
const SITEMAPS_DIR = 'sitemaps';

// Helper function to generate a random path
function randomPath(): string {
    const chars = 'abcdefghijklmnopqrstuvwxyz';
    const pathLength = Math.floor(Math.random() * 10) + 1;
    let path = '';
    for (let i = 0; i < pathLength; i++) {
        path += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return path;
}

// Function to generate a sample XML content with multiple random URLs
function generateXMLContent(index: number): string {
    // const urlCount = Math.floor(Math.random() * 100000) + 1;
    const urlCount = 100000;
    let urls = '';

    for (let i = 0; i < urlCount; i++) {
        urls += `
    <url>
        <loc>https://www.example.com/${randomPath()}-${index}-${i}</loc>
        <lastmod>2023-10-09</lastmod>
        <changefreq>monthly</changefreq>
        <priority>0.8</priority>
    </url>`;
    }

    return `<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
${urls}
</urlset>`;
}

function gzipAndSave(content: string, gzippedFilePath: string): Promise<void> {
    return new Promise((resolve, reject) => {
        const gzip = createGzip();
        const writeStream = createWriteStream(gzippedFilePath);

        // Handle the completion of the write stream
        writeStream.on('finish', resolve);

        // Handle any errors during the write process
        writeStream.on('error', reject);

        // Start the gzipping and writing process
        gzip.pipe(writeStream);
        gzip.write(content);
        gzip.end();
    });
}

export async function generateSitemaps(): Promise<void> {
    // Ensure the sitemaps directory exists
    if (!existsSync(SITEMAPS_DIR)) {
        mkdirSync(SITEMAPS_DIR);
    }

    // Generate and save sitemap files
    for (let i = 1; i <= 50; i++) {
        const filePath = join(SITEMAPS_DIR, `sitemap${i}.xml`);
        const gzippedFilePath = `${filePath}.gz`;
        if (!existsSync(gzippedFilePath)) { // Check if gzipped file exists before generating
            const content = generateXMLContent(i);

            await gzipAndSave(content, gzippedFilePath);
        // } else {
        //     console.log(`${gzippedFilePath} already exists. Skipping.`);
        }
    }

    console.log('Sitemap generation and gzipping completed!');
}